# Bài tập git-workflow
## Lý thuyết
    - tài liệu: https://www.makeareadme.com/
### kiến thức nắm được
    - git là gì, tại sao phải sử dụng git
    - cách làm việc nhóm. Chịu trách nhiệm trên từng commit
    
## thực hành
### Basic
    - tạo 1 repo trên gitlap
    - chia thành main và deverlop
    - up lên deverlop
    - tạo commit theo mẫu
    - merge deverlop vào main
    - viết README.md
### Advance
    - xử lý conflict
    - revert commit
    - cherry pick commit
    - reset commit
    - squash commit
## workflow